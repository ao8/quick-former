import XueInput from './input'
import XueTextarea from './textarea'
import XuePhone from './phone'
import XuePassword from './password'

export default {
  XueInput,
  XueTextarea,
  XuePhone,
  XuePassword,
}
