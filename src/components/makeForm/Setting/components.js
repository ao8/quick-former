import InputConfig from './input'
import TextareaConfig from './textarea'
import PhoneConfig from './phone'
import PasswordConfig from './password'

export default {
  InputConfig,
  TextareaConfig,
  PhoneConfig,
  PasswordConfig,
}
